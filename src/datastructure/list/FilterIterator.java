package datastructure.list;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

/**
 * Filter iterator
 */
public class FilterIterator<T> implements Iterator<T> {
	private List<T> list;
	private Predicate<T> predicate;
	private int cursor;
	private boolean wasRemoved;
	private boolean checkHasNext;
	private Iterator<T> iterator;
	

	// Constructor
	public FilterIterator(List<T> list, Predicate<T> predicate) {
		super();
		//this.filteredList = (List<T>) Stream.of(list).filter((Predicate<? super List<T>>) predicate).collect(Collectors.toList());
		this.list = list;	
		this.predicate = predicate;
		this.cursor = -1;
		this.wasRemoved = true;
		this.checkHasNext = false;
		this.iterator = list.iterator();
	}

	// public methods
	@Override
	public boolean hasNext() {
		return checkHasNext? true : jumpNextMatch(this.predicate);
	}

	@Override
	public T next() {
		if(hasNext()){
			this.checkHasNext = false;
			this.wasRemoved = false;
			return (T) this.list.get(this.cursor);
		}
		else
			throw new NoSuchElementException("The iteration has no more elements");
	}

	/**
	 * Removes from the underlying collection the last element returned by this iterator. 
	 * This method can be called only once per call to next(). 
	 * @throws UnsupportedOperationException when there is no last lelment
	 * returned by iterator
	 */
	@Override
	public void remove() {
		if(!this.wasRemoved){
			this.wasRemoved = true;
			this.iterator.remove();
			this.cursor--;
		}
		else
			throw new UnsupportedOperationException("You have to call next()");
	}
	
	// private methods	
	/**
	 * Jump to the next match element (use filter- predicate)
	 * @param predicate - filter 
	 * @return true if list contain next match element
	 */
	private boolean jumpNextMatch(Predicate<T> predicate){
		int step = 0;
		while(iterator.hasNext()){
			T element = iterator.next();
			step++;
			if(predicate.test(element)){
				this.cursor += step;
				this.checkHasNext = true;
				return true;
			}
		}
		return false;
	}
}
