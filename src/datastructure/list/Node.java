package datastructure.list;

/**
 * Component of linked list that stores a value and reference to the next element.
 */
public class Node<T> {
    /* (TODO Starterkit 1) Please introduce a sensible implementation */
	private T data;
	private Node<T> nextNode;
	
	// Getters and setters
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Node<T> getNextNode() {
		return nextNode;
	}

	public void setNextNode(Node<T> nextNode) {
		this.nextNode = nextNode;
	}
	
	// Constructors
	public Node(T data, Node<T> nextNode) {
		super();
		this.data = data;
		this.nextNode = nextNode;
	}
	
	public Node(T data) {
		super();
		this.data = data;
		this.nextNode = null;
	}
	
	public Node() {
		super();
		this.data = null;
		this.nextNode = null;
	}
	
	
	
	
}
