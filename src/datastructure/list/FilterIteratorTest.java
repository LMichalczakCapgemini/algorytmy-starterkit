package datastructure.list;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.function.Predicate;
import org.junit.Test;

public class FilterIteratorTest{	
	
	/**
	 * Test exception when remove twice the same element
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void Next_ExceptionRemoveTwiseAtRow() {
		String[] names = new String[] {"Adam", "Beata", "Klara", "Agata", "Kasia"};
		CustomArrayList<String> customList = new CustomArrayList<String>();	
		for (String name : names) 
			customList.add(name);
		Predicate<String> predicate = s -> s.startsWith("A");
		FilterIterator<String> sut = new FilterIterator<String>(customList, predicate);
		
		sut.next();
		sut.next();
		sut.remove();
		sut.remove();
	}
	
	/**
	 * Test that next returns correct elements
	 */
	@Test()
	public void Next_RturnCorrectElements() {
		String[] names = new String[] {"Adam", "Beata", "Klara", "Agata", "Kasia"};
		CustomArrayList<String> customList = new CustomArrayList<String>();	
		for (String name : names) 
			customList.add(name);
		Predicate<String> predicate = s -> s.startsWith("A");
		FilterIterator<String> sut = new FilterIterator<String>(customList, predicate);
		ArrayList<String> removedNames = new ArrayList<String>();
		
		while(sut.hasNext()){
			removedNames.add(sut.next());
		}
		
		assertTrue(removedNames.contains(names[0]));
		assertFalse(removedNames.contains(names[1]));
		assertFalse(removedNames.contains(names[2]));
		assertTrue(removedNames.contains(names[3]));
		assertFalse(removedNames.contains(names[4]));
	}
	
	/**
	 * Test that remove removes correct elements
	 */
	@Test()
	public void Remove_RemoveCorrectElements() {
		String[] names = new String[] {"Adam", "Beata", "Klara", "Agata", "Kasia"};
		CustomArrayList<String> customList = new CustomArrayList<String>();	
		for (String name : names) 
			customList.add(name);
		Predicate<String> predicate = s -> s.startsWith("A");
		int sizeAfterRemove;
		int sizeBeforeRemove = customList.size();
		FilterIterator<String> sut = new FilterIterator<String>(customList, predicate);
		
		while(sut.hasNext()){
			sut.next();
			sut.remove();
		}
		
		sizeAfterRemove = customList.size();
		assertTrue(sizeAfterRemove + 2 == sizeBeforeRemove);
		assertFalse(customList.contains(names[0]));
		assertTrue(customList.contains(names[1]));
		assertTrue(customList.contains(names[2]));
		assertFalse(customList.contains(names[3]));
		assertTrue(customList.contains(names[4]));
	}
	
	/**
	 * Test that iterator throws exception when there is no element returned by next()
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void Remove_ThrowsExceptionWhenThereWasNoNext() {
		CustomArrayList<String> customList = new CustomArrayList<String>();	
		Predicate<String> predicate = s -> s.startsWith("A");
			
		FilterIterator<String> sut = new FilterIterator<String>(customList, predicate);
		
		sut.remove();
	}
	
	
	/**
	 * Test that iterator filters a given list
	 */
	@Test
	public void FilterIteraror_FilterList() {
		String[] names = new String[] {"Adam", "Beata", "Klara", "Agata", "Kasia"};
		String[] nextStrings = new String[2];
		CustomArrayList<String> customList = new CustomArrayList<String>();	
		for (String name : names) 
			customList.add(name);
		Predicate<String> predicate = s -> s.startsWith("A");
			
		FilterIterator<String> sut = new FilterIterator<String>(customList, predicate);
		
		for(int i = 0; sut.hasNext(); i++)
			nextStrings[i] = sut.next();

		assertEquals(names[0], nextStrings[0]);
		assertEquals(names[3], nextStrings[1]);
		assertEquals(2, nextStrings.length);
	}
	
	

}
