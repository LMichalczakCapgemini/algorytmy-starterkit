package datatype;

import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.Test;

import datastructure.list.CustomArrayList;
import datastructure.list.CustomLinkedList;

public class CustomStackTest {
	
	/**
	 * Test that works as a LIFO for pop
	 */
	@Test
	public void Pop_LIFO() {
		CustomStack<Integer> sut = new CustomStack<Integer>(new CustomLinkedList<Integer>());
		int exampleInts[] = new int[]{0,1,2};
		int popedInts[] = new int[3];
		for (int i : exampleInts) 
			sut.push(i);
		
		for(int i=0; sut.size() > 0; i++)
			popedInts[i] = sut.pop();
		
		assertEquals(exampleInts[2], popedInts[0]);
		assertEquals(exampleInts[1], popedInts[1]);
		assertEquals(exampleInts[0], popedInts[2]);
	}
	
	/**
	 * Test that pop return element
	 */
	@Test
	public void Pop_ReturnElement() {
		CustomStack<Integer> sut = new CustomStack<Integer>(new CustomLinkedList<Integer>());
		int exampleInt = 1;
		int popedInt;
		sut.push(exampleInt);
		
		popedInt = sut.pop();
		
		assertEquals(exampleInt, popedInt);
	}
	
	/**
	 * Test that pop removes element
	 */
	@Test
	public void Pop_RemovesElement() {
		CustomStack<Integer> sut = new CustomStack<Integer>(new CustomLinkedList<Integer>());
		sut.push(1);
		int beforePop, afterPop;
		beforePop = sut.size();
		
		sut.pop();
		
		afterPop = sut.size();
		assertEquals(0, afterPop);
		assertEquals(1, beforePop);
	}
	
	/**
	 * Test that pop returns null when Stack is empty
	 */
	@Test(expected = EmptyStackException.class)
	public void Pop_NullWhenEmpty() {
		CustomStack<Integer> sut = new CustomStack<Integer>(new CustomLinkedList<Integer>());
		
		sut.pop();
	}
	
	
	/**
	 * Test that Stack adds elements
	 */
	@Test
	public void Push_AddsElements() {
		CustomStack<Integer> sut = new CustomStack<Integer>(new CustomLinkedList<Integer>());
		boolean isEmpty;
		int size;

		for(int i=1; i<=3; i++)
			sut.push(i);
		
		isEmpty = sut.isEmpty();
		size = sut.size();
		assertFalse(isEmpty);
		assertEquals(3, size);
	}
	
	
	/**
	 * Test that isEmpty returns false when Stack is not empty
	 */
	@Test
	public void IsEmpty_ReturnFalseWhenIsNotEmpty() {
		CustomLinkedList<Integer> list = new CustomLinkedList<>();
		list.add(1);
		CustomStack<Integer> sut = new CustomStack<Integer>(list);
		boolean isEmpty;
		sut.push(1);
		
		isEmpty = sut.isEmpty();
		
		assertFalse(isEmpty);
	}
	
	
	/**
	 * Test that isEmpty returns true when Stack is empty
	 */
	@Test
	public void IsEmpty_ReturnsTrueWhenIsEmpty() {
		CustomStack<Integer> sut = new CustomStack<Integer>(new CustomLinkedList<Integer>());
		CustomStack<Integer> sut2 = new CustomStack<Integer>(new CustomArrayList<Integer>());
		boolean isEmpty, isEmpty2;
		
		isEmpty = sut.isEmpty();
		isEmpty2 = sut2.isEmpty();
		
		assertTrue(isEmpty);
		assertTrue(isEmpty2);
	}
	
	/**
	 * Test that Stack using both types of lists returns correct size;
	 */
	@Test
	public void Size_ReturnsCorrectSize() {
		CustomStack<Integer> sut = new CustomStack<Integer>(new CustomLinkedList<Integer>());
		CustomStack<Integer> sut2 = new CustomStack<Integer>(new CustomArrayList<Integer>());
		int size, size2;
		for(int i=1; i<7; i++){
			sut.push(i);
			sut2.push(i);
		}
		sut2.push(0);
		
		size = sut.size();
		size2 = sut2.size();
		
		assertEquals(6, size);
		assertEquals(7, size2);
	}
	
	/**
	 * Test that not empty Stack returns size different to 0
	 */
	@Test
	public void Size_NotEmptyNotReturnsZero() {
		CustomStack<Integer> sut = new CustomStack<Integer>(new CustomLinkedList<Integer>());
		int size;
		sut.push(1);
		
		size = sut.size();
		
		assertNotEquals(0, size);
	}
	
	/**
	 * Test that empty Stack returns size equal to 0
	 */
	@Test
	public void Size_EmptyReturnsZero() {
		CustomStack<Integer> sut = new CustomStack<Integer>(new CustomLinkedList<Integer>());
		int size;
		
		size = sut.size();
		
		assertEquals(0, size);
	}

}
