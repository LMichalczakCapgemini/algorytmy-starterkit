package datastructure.list;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

public class CustomArrayListTest {
	
	
	/**
	 * Test Iterator
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void Iterator_removeException() {
		CustomLinkedList<Integer> sut = new CustomLinkedList<Integer>();
		sut.add(1);
		Iterator<Integer> iter = sut.iterator();
		
		
		iter.remove();
	}		
	
	/**
	 * Test Iterator
	 */
	@Test
	public void Iterator_remove() {
		CustomLinkedList<Integer> sut = new CustomLinkedList<Integer>();
		sut.add(1);
		Iterator<Integer> iter = sut.iterator();
		int sizeBeforeRemove = sut.size();
		
		iter.next();
		iter.remove();

		int sizeAfterRemove = sut.size();
		assertTrue(sizeAfterRemove + 1 == sizeBeforeRemove);
	}	
	
	/**
	 * Test Iterator
	 */
	@Test
	public void Iterator_Next() {
		CustomLinkedList<Integer> sut = new CustomLinkedList<Integer>();
		sut.add(1);
		int nextInt;
		Iterator<Integer> iter = sut.iterator();

		nextInt = iter.next();

		assertEquals(1, nextInt);
	}	
	
	/**
	 * Test Iterator
	 */
	@Test
	public void Iterator_hasNext(){
		CustomArrayList<Integer> sut = new CustomArrayList<Integer>(0);
		Iterator<Integer> iter = sut.iterator();
		boolean hasNext;
		sut.add(1);
		
		hasNext = iter.hasNext();
		
		assertEquals(true, hasNext);
	}
	
	
	/**
	 * Test Copy Array
	 */
	@Test
	public void RelocateToBIgger_ArrayWasCopy(){
		CustomArrayList<Integer> sut = new CustomArrayList<Integer>(0);
		for(int i = 0; i < 10; i++)
			sut.add(i);
		
		sut.add(11);
		
		assertEquals(1, (int) sut.get(1));
	}
	
	
	/**
	 * Test Relocate to smaller
	 */
	@Test
	public void RelocateToSmaller_DecreaseWhen60percent(){
		CustomArrayList<Integer> sut = new CustomArrayList<Integer>(0);
		for(int i = 0; i <= 10; i++)
			sut.add(i);
		
		for(int i = 9; i >=0; i--)
			sut.remove(i);
	}
	
	
	/**
	 * Test Relocate to bigger
	 */
	@Test
	public void RelocateToBIgger_IncreaseWhen90percent(){
		CustomArrayList<Integer> sut = new CustomArrayList<Integer>(0);
		for(int i = 0; i < 10; i++)
			sut.add(i);
		
		sut.add(11);
	}
	
	
	
	/**
	 * Test add Null 
	 */
	@Test
	public void AddIndex_AddNull(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String strings[] = {"a", "b", "c"};
		for (String string : strings) 
			sut.add(string);
		String testString = null;
		
		sut.add(0, testString);
		
		assertEquals(null, sut.get(0));
	}
	
	/** 
	 * Test that Set method throws exception when index is out of range - overMax
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void AddIndex_ExceptionWhenIndexOutOfRangeMax(){
		CustomArrayList<String> sut = new CustomArrayList<String>();

		sut.add(1, "TestString");
	}
	
	/** 
	 * Test that Set method throws exception when index is out of range - under 0
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void AddIndex_ExceptionWhenIndexOutOfRangeMin(){
		CustomArrayList<String> sut = new CustomArrayList<String>();

		sut.add(-1, "TestString");
	}
	
	/**
	 * Test that addIndex increase count of elements by 1 element
	 */
	@Test
	public void AddIndex_IncreaseArryList(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String strings[] = {"a", "b", "c"};
		for (String string : strings) 
			sut.add(string);
		String testString = "t";
		int sizeBeforeAdd = sut.size();
		int sizeAfterAdd;
		
		sut.add(0, testString);
		sizeAfterAdd = sut.size();
		
		assertEquals(sizeBeforeAdd + 1, sizeAfterAdd);
	}
	
	/**
	 * Test that addIndex shifts elements
	 */
	@Test
	public void AddIndex_Shift(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String strings[] = {"a", "b", "c"};
		for (String string : strings) 
			sut.add(string);
		String testString = "t";
		
		sut.add(0, testString);
		
		assertEquals(1, sut.indexOf(strings[0]));
		assertEquals(2, sut.indexOf(strings[1]));
		assertEquals(3, sut.indexOf(strings[2]));
	}
	
	/**
	 * Test that addIndex add element to correct index
	 */
	@Test
	public void AddIndex_AddToCorrectIndex(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String strings[] = {"a", "b", "c"};
		for (String string : strings) 
			sut.add(string);
		String testString = "t";
		
		sut.add(1, testString);
		
		assertEquals(1, sut.indexOf(testString));
	}
	
	
	/**
	 * Test remove returns false when we pass null as argument
	 */
	@Test
	public void RemoveObject_FalseWhenPassNull(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		boolean removeReturn;
		
		removeReturn = sut.remove(null);
		
		assertEquals(false, removeReturn);
	}
	
	/**
	 * Test remove returns false when object does not exist
	 */
	@Test
	public void RemoveObject_FasleIfNotExists(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		boolean removeReturn;
		
		removeReturn = sut.remove("TestString");
		
		assertEquals(false, removeReturn);
	}
	
	/**
	 * Test remove returns true when object exists
	 */
	@Test
	public void RemoveObject_TrueIfExists(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		boolean removeReturn;
		String testString = "TestString";
		sut.add(testString);
		
		removeReturn = sut.remove(testString);
		
		assertEquals(true, removeReturn);
	}
	
	/**
	 * After remove arrayList has one less element
	 */
	@Test
	public void RemoveObject_LessElementsAfterReomve(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		sut.add(testString);
		sut.add(testString);
		int sizeBeforeRemove = sut.size();
		int sizeAfterRemove;
		
		sut.remove(testString);
		sizeAfterRemove = sut.size();
		
		assertEquals(sizeBeforeRemove, sizeAfterRemove + 1);
	}
	
	/**
	 * test that remove(obj) removes correct object
	 */
	@Test
	public void RemoveObject_RemoveGoodObject(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String testString2 = "testString2";
		sut.add(testString);
		sut.add(testString2);
		
		sut.remove(testString2);
		
		assertEquals(sut.contains(testString), true);
		assertEquals(sut.contains(testString2), false);
	}

	/**
	 * Test that get method throws exception when index is out of range
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void RemoveIndex_ExceptionWhenIncorrectIndex(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		
		sut.remove(1);
	}
	
	/**
	 * test that after remove arrayList has one less parameter
	 */
	@Test
	public void RemoveIndex_LessElementsAfterRemove(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		sut.add(testString);
		sut.add(testString);
		int sizeBeforeRemove = sut.size();
		int sizeAfterRemove;
		
		sut.remove(1);
		sizeAfterRemove = sut.size();
		
		assertEquals(sizeBeforeRemove, sizeAfterRemove + 1);
	}
	
	/**
	 * test that remove removes correct object
	 */
	@Test
	public void RemoveIndex_RemoveGoodObject(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String testString2 = "testString2";
		String removedString;
		sut.add(testString);
		sut.add(testString2);
		
		removedString = sut.remove(1);
		
		assertEquals(testString2, removedString);
	}
	
	/**
	 * test that arrayList does not contain object after remove
	 */
	@Test
	public void RemoveIndex_DoesNotContainRemovedObject(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String testString2 = "testString2";
		sut.add(testString);
		sut.add(testString2);
		boolean containsBeforeRemove = sut.contains(testString2);
		boolean containsAfterRemove;
		
		sut.remove(1);
		containsAfterRemove = sut.contains(testString2);
		
		assertEquals(true, containsBeforeRemove);
		assertEquals(false, containsAfterRemove);
	}
	
	
	/**
	 * Test that clear removes all elements from arrayList
	 */
	@Test 
	public void clear_newEmptyArrayList(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String testString2 = "testString2";
		sut.add(testString);
		sut.add(testString2);
		
		sut.clear();

		assertEquals(true, sut.isEmpty());
		assertEquals(0, sut.size());
	}
	
	/**
	 * Test method in case of null parameter
	 */
	@Test
	public void IndexOf_NullPassed(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		int index;

		index = sut.indexOf(null);
		
		assertEquals(-1, index);
	}
	
	/**
	 * Test that indexOf returns -1 when object does not exist
	 */
	@Test
	public void IndexOf_Minus1WhenNotContain(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String testString2 = "testString2";
		int index;

		sut.add(testString);
		index = sut.indexOf(testString2);
		
		assertEquals(-1, index);
	}
	
	/**
	 * Test that IndexOf returns correct indexes
	 */
	@Test
	public void IndexOf_CorrectIndex(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String testString2 = "testString2";
		int index;
		int index2;

		sut.add(testString);
		sut.add(testString2);
		index = sut.indexOf(testString);
		index2 = sut.indexOf(testString2);
		
		assertEquals(0, index);
		assertEquals(1, index2);
	}
	
	/** 
	 * Test that Set method throws exception when index is out of range
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void Set_ExceptionWhenIncorrectIndex(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		
		sut.set(0, testString);
	}
	
	/**
	 * Test that set replace elements
	 */
	@Test 
	public void Set_ReplaceElements(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String previousString = "testString";
		String newString = "testString2";
		String replecedString;
		
		sut.add(previousString);
		
		replecedString =  sut.set(0, newString);
		
		assertEquals(previousString, replecedString);
		assertEquals(newString, sut.get(0));
	}
	
	/**
	 * Test that get method throws exception when index is out of range
	 */
	@SuppressWarnings("unused")
	@Test(expected = IndexOutOfBoundsException.class)
	public void Get_ExceptionWhenUncorrectIndex(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String testString2 = "testString2";
		String getString;
		
		sut.add(testString);
		sut.add(testString2);
		getString = sut.get(2);
	}
	
	/**
	 * Test that element element passed to second place is truly different to the first
	 */
	@Test
	public void GetDifferentElement_FailureWhenUncorrectIndex(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String testString2 = "testString2";
		String getString;
		
		sut.add(testString);
		sut.add(testString2);
		getString = sut.get(1);
		
		assertNotEquals(testString, getString);
	}
	
	/**
	 * Test that get returns from first place element equal to that what was passed
	 */
	@Test
	public void GetFromSecondPlace_SuccedWhenCorrectIndex(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String testString2 = "testString2";
		String getString;
		
		sut.add(testString);
		sut.add(testString2);
		getString = sut.get(1);
		
		assertEquals(testString2, getString);
	}
	
	/**
	 * Test that get returns from first place element equal to that what was passed
	 */
	@Test
	public void GetFromHead_SuccedWhenCorrectIndex(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String testString = "testString";
		String getString;
		
		sut.add(testString);	
		getString = sut.get(0);
		
		assertEquals(testString, getString);
	}
	
	/**
	 * Test contains method when we pass null as argument
	 */
	@Test
	public void Contains_FalseIfPassNull(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		boolean contains;
		String testString = "testString";
		sut.add(testString);
		
		contains = sut.contains(null);
		assertEquals(false, contains);
	}
	
	
	/**
	 * Test that contains return true when array contains wanted element
	 */
	@Test
	public void Contains_TrueWhenItContains(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		boolean contains;
		String objectToFind = "Test";
		String differentObject1 = "other1";
		String differentObject2 = "other2";
			
		sut.add(differentObject1);
		sut.add(objectToFind);
		sut.add(differentObject2);
		contains = sut.contains(objectToFind);
		
		assertEquals(true, contains);
	}
	
	/**
	 * Test that contains return false when array does not contain wanted element
	 */
	@Test
	public void Contains_FalseWhenItDoesNotContain(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		boolean contains;
		String objectToFind = "Test";
		String differentObject1 = "other1";
		String differentObject2 = "other2";
				
		sut.add(differentObject1);
		sut.add(differentObject2);
		contains = sut.contains(objectToFind);
		
		assertEquals(false, contains);
	}

	
	/**
	 * ArrayList is empty
	 */
	@Test
	public void IsEmpty_TrueWhenItIs(){
		CustomArrayList<Integer> sut = new CustomArrayList<Integer>();
		boolean isEmpty;
		
		isEmpty = sut.isEmpty();

		assertEquals(true, isEmpty);
	}
	
	/**
	 * ArrayList is not empty
	 */
	@Test
	public void IsEmpty_FalseWhenItIsNot(){
		CustomArrayList<Integer> sut = new CustomArrayList<Integer>();
		boolean isEmpty;
		
		sut.add(10);
		isEmpty = sut.isEmpty();

		assertEquals(false, isEmpty);
	}
	
	
	/**
	 * Add method adds example integer object, ArrayList has one more element
	 */
	@Test
	public void AddOneIntT_SucceedAndIncreaseCountOfElements() {
		// Arrange
		CustomArrayList<Integer> sut = new CustomArrayList<Integer>();
		boolean succeedAdd;
		int countBeforeAdd = sut.size();
		int countAfterAdd;

		// Act
		succeedAdd = sut.add(10);
		countAfterAdd = sut.size();
		
		// Assert
		assertEquals(true, succeedAdd);
		assertEquals(false, sut.isEmpty());
		assertTrue(countBeforeAdd +1 == countAfterAdd);
	}

	/**
	 * Add method adds four example strings objects, ArrayList has four more elements
	 */
	@Test
	public void AddFourStrings_FourMoreElementsInArrayList(){
		CustomArrayList<String> sut = new CustomArrayList<String>();
		String strings[] = {"Str1", "Str2", "Str3", "Str4"};
		int countBeforeAdd = sut.size();
		int countAfterAdd;
		
		for (String string : strings) {
			sut.add(string);
		}
		countAfterAdd = sut.size();
		
		assertEquals(false, sut.isEmpty());
		assertTrue(countBeforeAdd +4 == countAfterAdd);
	}
	
	
	/**
	 * Default constructor initialize default size of array
	 */
	@Test
	public void ConstructWithDefaultCapacity_InitialValuesAsExpected() {
		// Arrange
		CustomArrayList<Integer> sut;
		//final int DEFAULT_CAPACITY = 10;

		// Act
		sut = new CustomArrayList<Integer>();

		// Assert
		//assertEquals(DEFAULT_CAPACITY, sut.getArray().length);	//TODO jak to sprawdzic bez getterow
		//assertEquals(DEFAULT_CAPACITY, sut.getFullSize());
		assertEquals(0, sut.size());
		assertEquals(true, sut.isEmpty());
	}

	/**
	 * Custom constructor initialize custom size of array
	 */
	@Test
	public void ConstructWithCustomCapacity_InitialValuesAsExpected() {
		// Arrange
		CustomArrayList<Integer> sut;
		int customCapacity = 34;

		// Act
		sut = new CustomArrayList<Integer>(customCapacity);

		// Assert
		//assertEquals(customCapacity, sut.getArray().length);
		//assertEquals(customCapacity, sut.getFullSize());
		assertEquals(0, sut.size());
		assertEquals(true, sut.isEmpty());
	}

}
