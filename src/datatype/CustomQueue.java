package datatype;

import java.util.List;

/**
 * FIFO queue
 * Adds an the end of list
 * Takes from head of list
 */
public class CustomQueue<T> extends AbstractCustomQueueAdapter<T> {
    List<T> storage = null;

    // Constructor
    public CustomQueue(List<T> storage) {
        this.storage = storage;
    }

    @Override
    public int size() {
        return this.storage.size();
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public boolean add(T t) {
    	return this.storage.add(t);
    }

    /**
     * Retrieves and removes the head of this queue, or returns null if this queue is empty. 	
     * @return the head of this queue, or null if this queue is empty
     */
    @Override
    public T poll() {
    	try {
    		return this.storage.remove(0);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
    }

    /**
     * Retrieves, but does not remove, the head of this queue, or returns null if this queue is empty.
     * @return the head of this queue, or null if this queue is empty
     */
    @Override
    public T peek() {
    	try {
    		return this.storage.get(0);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
		
        
    }
}
