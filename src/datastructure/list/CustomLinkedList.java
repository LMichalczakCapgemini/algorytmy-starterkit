package datastructure.list;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * List based on recursively related objects
 *
 * @param <T>
 */
public class CustomLinkedList<T> extends AbstractCustomListAdapter<T> {
	private Node<T> head;
	private Node<T> tail;
	private int size;

	// Constructors
	public CustomLinkedList() {
		super();
		clear();
	}

	// Public methods

	/**
	 * Return Number of elements of CustomLinkedList
	 * 
	 * @return Number of elements of CustomLinkedList
	 */
	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	/**
	 * Returns true if this list contains the specified element. 
	 * @param o element whose presence in this list is to be tested
	 * @return true if this list contains the specified element
	 */
	@Override
	public boolean contains(Object o) {
		CustomLinkedListIterator<T> iterator = new CustomLinkedListIterator<T>();

		while (iterator.hasNext()) {
			if (Objects.equals(iterator.next(), o))
				return true;
		}

		return false;
	}

	@Override
	public Iterator<T> iterator() {
		return new CustomLinkedListIterator<T>();
	}

	/**
	 * Appends the specified element to the end of this list.
	 * @param t element to be appended to this list
	 */
	@Override
	public boolean add(T t) {
		Node<T> newNode = new Node<T>(t);

		if (isEmpty())
			this.tail = this.head = newNode;
		else{
			this.tail.setNextNode(newNode);
			this.tail = newNode;
		}

		this.size++;
		return true;
	}

	/**
	 * Removes the first occurrence of the specified element from this list, if
	 * it is present Set new tail Decrement size
	 * @param o element to be removed from this list, if present
	 * @return true if this list contained the specified element
	 */
	@Override
	public boolean remove(Object o) {
		if (Objects.equals(this.head.getData(), o)) {
			this.head = this.head.getNextNode();
			this.size--;
			return true;
		}

		Node<T> currentNode = new Node<T>(null, head);
		while (currentNode.getNextNode().getNextNode() != null) {
			if (Objects.equals(currentNode.getNextNode().getNextNode().getData(), o)) {
				if (currentNode.getNextNode().getNextNode().getNextNode() == null)
					this.tail = currentNode.getNextNode();
				currentNode.getNextNode().setNextNode(currentNode.getNextNode().getNextNode().getNextNode());
				this.size--;
				return true;
			}
			currentNode.setNextNode(currentNode.getNextNode().getNextNode());
		}

		return false;
	}

	@Override
	public void clear() {
		this.head = null;
		this.tail = null;
		this.size = 0;
	}

	/**
	 * Returns the element at the specified position in this list.
	 * @param index index of the element to return
	 * @return the element at the specified position in this list
	 */
	@Override
	public T get(int index) {
		validateIndex(index);
		
		int counter = 0;
		Node<T> currentNode = new Node<T>(null, this.head);
		while (counter != index) {
			currentNode.setNextNode(currentNode.getNextNode().getNextNode());
			counter++;
		}
		return currentNode.getNextNode().getData();
	}
	/**
	 * Replaces the element at the specified position in this list with the specified element.
	 * @param element element to be stored at the specified position
	 * @return the element previously at the specified position
	 */
	@Override
	public T set(int index, T element) {
		validateIndex(index);
		
		int counter = 0;
		Node<T> currentNode = new Node<T>(null, this.head);
		while (counter != index) {
			currentNode.setNextNode(currentNode.getNextNode().getNextNode());
			counter++;
		}
		T data = currentNode.getNextNode().getData();
		currentNode.getNextNode().setData(element);
		return data;
	}

	/**
	 * Inserts the specified element at the specified position in this list. 
	 * Shifts the element currently at that position (if any) and any subsequent elements to the right
	 * If add at last position - set new tail
	 * @param index index at which the specified element is to be inserted
	 * @param element element to be inserted
	 */
	@Override
	public void add(int index, T element) {
		validateIndexAdd(index);

		Node<T> newNode = new Node<T>(element);
		int counter = 1;
		Node<T> currentNode = new Node<T>(null, this.head);

		if (index == 0) {
			newNode.setNextNode(this.head);
			this.head = newNode;

		} else {
			while (counter  != index) {
				currentNode.setNextNode(currentNode.getNextNode().getNextNode());
				counter++;
			}
			newNode.setNextNode(currentNode.getNextNode().getNextNode());
			currentNode.getNextNode().setNextNode(newNode);
		}
		if(index == size)
			this.tail = newNode;
		size++;
	}

	/**
	 * Removes the element at the specified position in this list. 
	 * Shifts any subsequent elements to the left (subtracts one from their indices). 
	 * Returns the element that was removed from the list.
	 * @param index the index of the element to be removed
	 * @return the element previously at the specified position
	 */
	@Override
	public T remove(int index) {
		validateIndex(index);
		
		T data;
		int counter = 1;
		Node<T> currentNode = new Node<T>(null, this.head);


		if(index == 0){
			data = this.head.getData();
			this.head = this.head.getNextNode();
		}
		else{
			while(counter != index){
				currentNode.setNextNode(currentNode.getNextNode().getNextNode());
				counter++;
			}
			data = currentNode.getNextNode().getNextNode().getData();
			currentNode.getNextNode().setNextNode(currentNode.getNextNode().getNextNode().getNextNode());
			if(index == size -1)
				this.tail= currentNode.getNextNode();
		}
		this.size--;
		return data;
	}

	/**
	 * Returns the index of the first occurrence of the specified element in this list,
	 *  or -1 if this list does not contain the element.
	 *  @param o element to search for
	 *  @return the index of the first occurrence of the specified element in this list, 
	 *  or -1 if this list does not contain the element
	 */
	@Override
	public int indexOf(Object o) {
		if(isEmpty())
			return -1;
		
		if(Objects.equals(this.head.getData(), o))
			return 0;
		
		Node<T> currentNode = new Node<T>(null, this.head);
		int counter = 0;
		
		while(currentNode.getNextNode().getNextNode() != null){
			currentNode.setNextNode(currentNode.getNextNode().getNextNode());
			counter++;
			if(Objects.equals(currentNode.getNextNode().getData(), o))
				return counter;
		}

		return -1;
	}

	// Private methods
	/**
	 * Validate if index is not out of range to do operations at element
	 * @param index
	 * @throws IndexOutOfBoundsException if the index is out of range (index < 0 || index >= size())
	 */
	private void validateIndex(int index) {
		if (index < 0 || index >= this.size)
			throw new IndexOutOfBoundsException("index is out of range");
	}
	
	/**
	 * Validate if index is not out of range to do add new element
	 * @param index
	 * @throws IndexOutOfBoundsException if the index is out of range (index < 0 || index > size())
	 */
	private void validateIndexAdd(int index) {
		if (index < 0 || index > this.size)
			throw new IndexOutOfBoundsException("index is out of range");
	}
	
	/**
	 * Iterator for CustomLinkedList
	 */
	private class CustomLinkedListIterator<E> implements Iterator<E> {
		private Node<T> currentNode;
		private boolean wasRemoved;
		private int cursor;

		public CustomLinkedListIterator() {
			super();
			this.currentNode = new Node<T>(null, head);
			this.wasRemoved = true;
			this.cursor = -1;
		}

		@Override
		public boolean hasNext() {
			return currentNode.getNextNode()  != null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public E next() {
			if (hasNext()) {
				T data = currentNode.getNextNode().getData();
				currentNode.setNextNode(currentNode.getNextNode().getNextNode());
				this.wasRemoved = false;
				this.cursor++;
				return (E) data;
			} else
				throw new NoSuchElementException("The iteration has no more elements");
		}

		@Override
		public void remove() {
			/* (TODO Starterkit 1)Please introduce a sensible implementation */
			if (!wasRemoved) {
				CustomLinkedList.this.remove(this.cursor);
				this.wasRemoved = true;
				this.cursor--;
			} else
				throw new UnsupportedOperationException("You have to call next()");
		}
	}
}
