package datatype;

import static org.junit.Assert.*;

import org.junit.Test;

import datastructure.list.CustomArrayList;
import datastructure.list.CustomLinkedList;

public class CustomQueueTest {

	
	/**
	 * Test that works as a FIFO for peek
	 */
	@Test
	public void Peek_FIFO() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		int exampleInts[] = new int[]{0,1};
		int peekedInts[] = new int[2];

		sut.add(exampleInts[0]);
		peekedInts[0] = sut.peek();
		sut.add(exampleInts[1]);
		peekedInts[1] = sut.peek();
		
		assertEquals(exampleInts[0], peekedInts[0], peekedInts[1]);
	}
	
	
	/**
	 * Test the peek return null when queue is empty
	 */
	@Test
	public void Peek_NullWhenEmpty(){
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		boolean isEmpty = sut.isEmpty();
		Object returnFromPeek;
		
		returnFromPeek = sut.peek();
		
		assertEquals(null, returnFromPeek);
		assertEquals(true, isEmpty);
	}
	
	/**
	 * Test the peek does not remove element
	 */
	@Test
	public void Peek_NotRemoves(){
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		sut.add(1);
		int afterPeek;
		int beforePeek = sut.size();
		
		afterPeek = sut.peek();
		
		assertEquals(beforePeek, afterPeek);
	}
	
	/**
	 * Test that works as a FIFO for poll
	 */
	@Test
	public void Poll_FIFO() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		int exampleInts[] = new int[]{0,1,2};
		int polledInts[] = new int[3];
		for (int i : exampleInts) 
			sut.add(i);
		
		for(int i=0; sut.size() > 0; i++)
			polledInts[i] = sut.poll();
		
		assertEquals(exampleInts[0], polledInts[0]);
		assertEquals(exampleInts[1], polledInts[1]);
		assertEquals(exampleInts[2], polledInts[2]);
	}
	
	/**
	 * Test that pool return element
	 */
	@Test
	public void Poll_ReturnElement() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		int exampleInt = 1;
		int polledInt;
		sut.add(exampleInt);
		
		polledInt = sut.poll();
		
		assertEquals(exampleInt, polledInt);
	}
	
	/**
	 * Test that pool removes element
	 */
	@Test
	public void Poll_RemovesElement() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		sut.add(1);
		int beforePoll, afterPoll;
		beforePoll = sut.size();
		
		sut.poll();
		
		afterPoll = sut.size();
		assertEquals(0, afterPoll);
		assertEquals(1, beforePoll);
	}
	
	/**
	 * Test that pool returns null when queue is empty
	 */
	@Test
	public void Poll_NullWhenEmpty() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		
		assertEquals(null, sut.poll());
	}
	
	
	/**
	 * Test that queue adds elements
	 */
	@Test
	public void Add_AddsElements() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		boolean isEmpty;
		int size;

		for(int i=1; i<=3; i++)
			sut.add(i);
		
		isEmpty = sut.isEmpty();
		size = sut.size();
		assertFalse(isEmpty);
		assertEquals(3, size);
	}
	
	
	/**
	 * Test that isEmpty returns false when queue is not empty
	 */
	@Test
	public void IsEmpty_FalseWhenIsNotEmpty() {
		CustomLinkedList<Integer> list = new CustomLinkedList<>();
		list.add(1);
		CustomQueue<Integer> sut = new CustomQueue<Integer>(list);
		boolean isEmpty;
		sut.add(1);
		
		isEmpty = sut.isEmpty();
		
		assertFalse(isEmpty);
	}
	
	
	/**
	 * Test that isEmpty returns true when queue is empty
	 */
	@Test
	public void IsEmpty_TrueWhenIsEmpty() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		CustomQueue<Integer> sut2 = new CustomQueue<Integer>(new CustomArrayList<Integer>());
		boolean isEmpty, isEmpty2;
		
		isEmpty = sut.isEmpty();
		isEmpty2 = sut2.isEmpty();
		
		assertTrue(isEmpty);
		assertTrue(isEmpty2);
	}
	
	/**
	 * Test that queue using both types of lists returns correct size;
	 */
	@Test
	public void Size_ReturnsCorrectSize() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		CustomQueue<Integer> sut2 = new CustomQueue<Integer>(new CustomArrayList<Integer>());
		int size, size2;
		for(int i=1; i<7; i++){
			sut.add(i);
			sut2.add(i);
		}
		sut2.add(0);
		
		size = sut.size();
		size2 = sut2.size();
		
		assertEquals(6, size);
		assertEquals(7, size2);
	}
	
	/**
	 * Test that not empty queue returns size different to 0
	 */
	@Test
	public void Size_NotEmptyNotReturnsZero() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		int size;
		sut.add(1);
		
		size = sut.size();
		
		assertNotEquals(0, size);
	}
	
	/**
	 * Test that empty queue returns size equal to 0
	 */
	@Test
	public void Size_EmptyReturnsZero() {
		CustomQueue<Integer> sut = new CustomQueue<Integer>(new CustomLinkedList<Integer>());
		int size;
		
		size = sut.size();
		
		assertEquals(0, size);
	}

}
