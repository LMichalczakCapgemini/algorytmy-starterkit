package datastructure.list;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * List based on the table
 *
 * @param <T>
 */
public class CustomArrayList<T> extends AbstractCustomListAdapter<T>{

	private Object array[];
	private int fullSize;
	private int count;
	private final int DEFAULT_CAPACITY = 10;
	private final double MAX_BORDER = 0.9;
	private final double MIN_BORDER = 0.6;
	private final double RELOCATE_SIZE_PARAMETER = 0.5;

	

	// Constrctors
	public CustomArrayList(int initialCapacity) {
		this.array = new Object[initialCapacity];
		this.fullSize = initialCapacity;
		this.count = 0;
	}

	public CustomArrayList() {
		clear();
	}

	// Public methods
	@Override
	public int size() {
		return this.count;
	}

	@Override
	public boolean isEmpty() {
		return this.count == 0;
	}

	/** 
	 * Returns true if this list contains the specified element. More formally, 
	 * returns true if and only if this list contains at least one element e 
	 * such that (o==null ? e==null : o.equals(e))
	 * 
	 * @param o object to find in ArraList
	 * @return true if it contains or false if it does not contain
	 * @see datastructure.list.AbstractCustomListAdapter#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o) {
		return Stream.of(this.array).limit(this.count).anyMatch(object -> object.equals(o)); 
	} 

	@Override
	public Iterator<T> iterator() {
		return new CustomArrayListIterator<>();
	}

	/**
	 * Add element to first empty slot in array and increase count
	 * of elements
	 * 
	 * @param t Element to add
	 * @return True in case of succeed, else False
	 */
	@Override
	public boolean add(T t) {
		if(this.IsToSmall())
			this.RelocateToBigger();

		this.array[count] = t;
		this.count++;
		return true;
	}// TODO  biblioteki arrays

	/**
	 * Removes the first occurrence of the specified element from this list, 
	 * if it is present. If the list does not contain the element, it is unchanged
	 * @param o element to be removed from this list, if present
	 * @return true if this list contained the specified element
	 */
	@Override
	public boolean remove(Object o) {
		int index = indexOf(o);
		if(index == -1) 
			return false;
		
		if(IsToBig())
			RelocateToSmaller();
		
		for(int i = index; i < this.count; i++){
			this.array[i] = this.array[i+1];
		}
		this.array[this.count] = null;
		this.count--;
		
		return true;
	}

	/**
	 * Removes all of the elements from this list. The list will be empty after this call returns.
	 * List will has default capacity
	 */
	@Override
	public void clear() {
		this.array = new Object[DEFAULT_CAPACITY];
		this.count = 0;
		this.fullSize = DEFAULT_CAPACITY;
	}

	/**
	 * Returns the element at the specified position in this list.
	 * 
	 * @param index index of element to get
	 * @return the element at the specified position in this list
	 * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >= count)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T get(int index) {
		validateIndex(index);
		
		return (T) this.array[index];
	}

	/**
	 * Replaces the element at the specified position in this list with the specified element.
	 * @param index - index of the element to replace
 	 * @param element - element to be stored at the specified position
 	 * @return the element previously at the specified position
 	 * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >= count)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T set(int index, T element) {
		validateIndex(index);
		
		T previousElement = (T) this.array[index];
		this.array[index] = element;
		return previousElement;
	}

	/**
	 * Inserts the specified element at the specified position in this list. 
	 * Shifts the element currently at that position (if any) and any subsequent elements to the right
	 * 
	 * @param index index at which the specified element is to be inserted
	 * @param element element to be inserted
	 * @throws IndexOutOfBoundsException if the index is out of range (index < 0 || index > size())
	 */
	@Override
	public void add(int index, T element) {
		validateIndexAdd(index);
		
		if(this.IsToSmall())
			this.RelocateToBigger();
		
		for(int i = this.count; i > index; i--)
			this.array[i] = this.array[i-1];
		
		this.array[index] = element;
		this.count++;
	}

	/**
	 * Removes the element at the specified position in this list. Shifts any subsequent elements to the left
	 * 
	 * @param index the index of the element to be removed
	 * @return the element that was removed from the list
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T remove(int index) {
		validateIndex(index);
		
		if(IsToBig())
			RelocateToSmaller();
		
		T removedElement = (T) this.array[index];
		
		for(int i = index; i < this.count; i++){
			this.array[i] = this.array[i+1];
		}
		this.array[this.count] = null;
		this.count--;
		
		return removedElement;
	}

	/**
	 * Returns the index of the first occurrence of the specified element in this list, 
	 * or -1 if this list does not contain the element
	 * @param o element to search for
	 * @return the index of the first occurrence of the specified element in this list, 
	 * or -1 if this list does not contain the element
	 */
	@Override
	public int indexOf(Object o) {		
		if(!contains(o))
			return -1;
					
		int i = 0;
		for (Object object : array) {
			if (Objects.equals(o, object))
				return i;
			i++;
		}
		return -1;
	} 

	// Privte methods
	/**
	 * Check if we have to increase max size of array
	 * @param true when we have to relocate array
	 */
	private boolean IsToSmall(){
		return this.fullSize == 0 || 
				(double) this.count / this.fullSize >= MAX_BORDER;
	}
	
	private void RelocateToBigger(){
		if(this.fullSize <=1)
			this.fullSize = 2;
		else
			this.fullSize *= (1 + RELOCATE_SIZE_PARAMETER);
		Object[] newArray = new Object[this.fullSize];
		System.arraycopy(array, 0, newArray, 0, this.count);
		this.array = newArray;
	}
	
	private boolean IsToBig(){
		return (double) this.count / this.fullSize <= MIN_BORDER;
	}
	
	private void RelocateToSmaller(){
		this.fullSize *= 0.9;
		Object[] newArray = new Object[this.fullSize ];
		System.arraycopy(array, 0, newArray, 0, this.count);
		this.array = newArray;
	}
	
	/**
	 * 
	 * @param index index to validate
	 * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >= size())
	 */
	public void validateIndex(int index){
		if(index < 0 || index >= size())
			throw new IndexOutOfBoundsException("Index is out of range");
	}
	
	/**
	 * 
	 * @param index index to validate
	 * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index > size())
	 */
	public void validateIndexAdd(int index){
		if(index < 0 || index > size())
			throw new IndexOutOfBoundsException("index is out of range");
	}
	
	// Private class
	/**
	 * Iterator for CustomArrayList
	 */
	private class CustomArrayListIterator<E> implements Iterator<E> {
		private int cursor;
		private boolean wasRemoved;
		
		
		public CustomArrayListIterator() {
			super();
			this.cursor = -1;
			this.wasRemoved = true;
		}

		/**
		 * Returns true if the iteration has more elements. 
		 * (In other words, returns true if next() would return an element rather than throwing an exception.)
		 * @return true if the iteration has more elements
		 */
		@Override
		public boolean hasNext() {
			return this.cursor +1 < size();
		}

		/**
		 * Returns the next element in the iteration.
		 * @return the next element in the iteration
		 * @throws NoSuchElementException if the iteration has no more elements
		 */
		@SuppressWarnings("unchecked")
		@Override
		public E next() {
			if(hasNext()){
				wasRemoved = false;
				return (E) array[++this.cursor];
			}
			else
				throw new NoSuchElementException("The iteration has no more elements");
		}

		/**
		 * Removes from the underlying collection the last element returned by this iterator. 
		 * This method can be called only once per call to next(). 
		 * 
		 */
		@Override
		public void remove() {
			if(!wasRemoved){
				CustomArrayList.this.remove(this.cursor);
				this.wasRemoved = true;
				this.cursor--;
			}
			else
				throw new UnsupportedOperationException("You have to call next()");
		}
	}
}
