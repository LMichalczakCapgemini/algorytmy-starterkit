package datatype;

import java.util.EmptyStackException;
import java.util.List;

/**
 * Stack, LIFO queue
 * Adds and takes from the end of list
 */
public class CustomStack<T> extends AbstractCustomStackAdapter<T> {
    List<T> storage = null;

    public CustomStack(List<T> storage) {
        this.storage = storage;
    }

    // TODO: pytanie- w dokumentacji zwraza item
    /**
     * Pushes an item onto the top of this stack
     * @param t the item to be pushed onto this stack
     */
    @Override
    public void push(T t) { 
    	this.storage.add(t);
    }

    /**
     * Removes the object at the top of this stack and returns that object as the value of this function
     * @return The object at the top of this stack
     * @throws EmptyStackException if this stack is empty
     */
    @Override
    public T pop() {
    	try {
			return this.storage.remove(this.size() - 1);
		} catch (IndexOutOfBoundsException e) {	
			throw new EmptyStackException();
		}
    }

    @Override
    public int size() {
        return this.storage.size();
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
}
